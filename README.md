# Oh My Cost

## Getting started

`make`

## Output

```
/Users/josephburnett/work/oh-my-cost〉./bin/omc

 90.36 ┤ ╭╮
 75.02 ┤ │╰──╮
 59.68 ┤ │   │
 44.34 ┤ │   │
 28.99 ┼─╯   │
 13.65 ┤     ╰───────────────────────
             USW2-HostUsage:mac2

 78.58 ┤                    ╭────────
 74.48 ┤                    │
 70.37 ┤ ╭──────────────────╯
 66.27 ┤ │
 62.17 ┤╭╯
 58.06 ┼╯
           USW2-EBS:VolumeUsage.gp2

 26.00 ┼─────────────────────────────
             USE2-HostUsage:mac1

 2.87 ┼╮     ╭╮
 2.29 ┤│     ││
 1.72 ┤│     ││
 1.15 ┤│     ││
 0.57 ┤│     ││
 0.00 ┤╰─────╯╰─────────────────────
           EU-USW2-AWS-Out-Bytes

 1.44 ┤                    ╭────────
 1.25 ┤      ╭─────────────╯
 1.06 ┤      │
 0.87 ┤╭─────╯
 0.68 ┤│
 0.49 ┼╯
           USW2-EBS:SnapshotUsage

     DOLLARS PER DAY (30 days)
```
